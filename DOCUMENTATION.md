Aqui é aonde será explicado de como funciona seus métodos e seus requests

**POST /user/login** - Login do usuário

```hash
--body {
	"email":"eduardo@admin.com.br",
	"password": "123456"
}

--headers Content-Type: application/json
```

**POST /user/register** - Registrar um novo usuário, abaixo já são os atributos obrigatórios para informar.

```hash
--body {
	"name": {
		"first": "Eduardo",
		"last": "Melo"
	},
	"email": "eduardo@hotmail.com",
	"password": "12345678",
	"gender": "male",
	"birth_date": "25-10-98"

}

--headers Content-Type: application/json
```

## Ações de usuário comum

**DELETE /user** - Deletar sua conta de usuário.

```hash
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

**PUT /user** - Deletar sua conta de usuário. (no exemplo estou alterando seu e-mail, pode usar mais de um campo dentro do JSON)

```hash
--body { email: 'edu@gmail.com' }
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

**POST /medias/vote** - Votar no filme especifico.

```hash
--body: { id: '232138djsadjsa', vote: 2 }
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

**GET /medias** - Listar todos os filmes ou séries. (no exemplo estou filtrando pelo nome do filme, mas por padrão ele ja deixa filtrando como nome do filme, neste caso não precisa de declarar o filter.)

`genres` - Gêneros de filme
`studio` - Em qual estudio foi feito
`director` - Direção

```hash
--body: { content: "harry potter", filter: "name"  }
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

## Ações administrativas

**DELETE /medias** - Apaga o filme/série especifica.

```hash
--body: { id: "asdsadadas" }
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

**POST /medias** - Registrar um novo filme/ série.

```hash
--body: {
"name":"Harry Potter",
"type_media": "movie",
"thumbnail": "http://cdn.eduardo.com/imagem_random.png",
"description": "Sobre o filme",
"trailer": "http://media.eduardo.com/trailer_piratas_do_caribe.mkv",
"url_media": "http://media.eduardo.com/filme_piratas_do_caribe.mkv",
"min_age": 16,
"genres": "Ação",
"studio": "Walt Disney Pictures",
"director": "asdsads dsa dasasdsa"

}

--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```

**PUT /medias** - O quase o mesmo o anterior porém você pega pelo id e declara o atributo update que é dentro dele que joga os valores que quer alterar.

```hash
--body: { id: "sadsadsa", update: { email: "oi@oi.com" }}
--headers Content-Type: application/json
          Authorization: 'Bearer token_auth'
```
