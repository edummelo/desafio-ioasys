const { Users } = require('../models/index')
const { validationResult } = require('express-validator')
const { STATUS_REQUEST } = require('../constants')
module.exports = {
  login: async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[401].status).json({
        errorStatus: STATUS_REQUEST[401],
        errors: errors.array()
      })
    }

    const { email, password } = req.body

    const userLogged = await Users.checkLogin({ email, password })

    if (!userLogged) return res.status(STATUS_REQUEST[401].status).send(STATUS_REQUEST[401])

    res.status(200).json({
      status: 200,
      message: 'Has logged successfully',
      authentication: {
        token: await Users.generateToken({ id: userLogged._id, email, permissions: userLogged.permissions })
      }
    })
  },
  createAcc: async (req, res) => {
    const data = req.body
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[400].status).json({
        errorStatus: STATUS_REQUEST[400],
        errors: errors.array()
      })
    }

    data.created_at = new Date().getTime()
    const isExists = await Users.checkRegister(data)
    if (isExists) return res.status(STATUS_REQUEST[400].status).json({ status: 400, message: 'Email already exists' })

    if (!['male', 'female'].includes(req.body.gender)) return res.status(STATUS_REQUEST[400].status).json({ status: 400, message: 'Female or male' })
    const registered = await Users.createAccount(data)
    if (!registered) return res.status(STATUS_REQUEST[400].status).json(STATUS_REQUEST[400])

    res.status(200).json({
      message: 'registered success!',
      my_data: {
        id: registered._id,
        name: registered.name,
        permissions: registered.permissions,
        created_at: registered.created_at
      },
      authentication: {
        token: await Users.generateToken({ id: registered._id, email: data.email, permissions: registered.permissions })
      }
    })
  },

  deleteAcc: async (req, res) => {
    if (!req.idUser) return res.status(400).json({ messsage: 'You are not logged' })
    await Users.deleteAccount(req.idUser)
    res.status(200).json({ message: 'Account deleted!' })
  },

  updateAcc: async (req, res) => {
    if (req.idUser) return res.status(400).json({ messsage: 'Id invalid' })
    const { data } = req.body
    await Users.updateAccount(req.idUser, data)
  }
}
