const { registerMedia, searchMedia, deleteMedia, verifyMedias, updateMedias, voteMedia } = require('../models/medias.model')
const { STATUS_REQUEST } = require('../constants')
const { validationResult } = require('express-validator')
const { Types } = require('mongoose')
const { Votes } = require('../models/index')
module.exports = {
  register: async (req, res) => {
    const body = req.body
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[400].status).json({
        errorStatus: STATUS_REQUEST[400],
        errors: errors.array()
      })
    }

    if (await verifyMedias({ name: body.name })) return res.status(STATUS_REQUEST[400].status).json({ message: 'Movie already exists' })
    const registered = await registerMedia(body)
    if (!registered) return res.status(STATUS_REQUEST[401].status).json(STATUS_REQUEST[401])

    return res.status(200).json({
      status: 200,
      message: 'Registered successfully',
      data: registered
    })
  },

  view: async (req, res) => {
    let { content, filter } = req.body
    if (!['name', 'genres', 'studio', 'director'].includes(filter)) filter = false
    const contents = await searchMedia(content, filter || false)
    return res.status(200).json({
      status: 200,
      search: {
        content,
        filter: filter || false
      },
      results: contents
    })
  },

  deleteMedias: async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[400].status).json({
        errorStatus: STATUS_REQUEST[400],
        errors: errors.array()
      })
    }

    const { id } = req.body
    const hasDeleted = await deleteMedia({ id })
    if (!hasDeleted) return res.status(404).json({ message: 'Media not found!' })
    return res.status(200).json({
      message: 'Deleted succesfully data'
    })
  },
  updateMedias: async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[400].status).json({
        errorStatus: STATUS_REQUEST[400],
        errors: errors.array()
      })
    }

    const { update, id } = req.body
    if (!Types.ObjectId.isValid(id)) return res.status(400).json({ message: 'Id is invalid!' })
    if (!(await verifyMedias({ _id: new Types.ObjectId(id) }))) return res.status(404).json({ message: 'Media not found!' })

    const updated = await updateMedias(id, update)
    if (!updated) return res.status(401).json({ message: 'Id is invalid.' })
    return res.status(200).json({
      message: 'Updated successfully!',
      update: updated
    })
  },

  voteMedias: async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(STATUS_REQUEST[400].status).json({
        errorStatus: STATUS_REQUEST[400],
        errors: errors.array()
      })
    }
    if (!req.idUser) return res.status(STATUS_REQUEST[401].status).json(STATUS_REQUEST[401])
    const { id, vote } = req.body

    const statusMessage = {
      message: 'voted successfully!',
      data: 'you already voted ' + vote
    }
    const idUser = req.idUser
    const votesVerify = await Votes.verifyVoted({ idUser, vote, id })
    if (votesVerify.status) return res.status(200).json(statusMessage)
    if (isNaN(vote)) return res.status(400).json({ message: 'Vote undefined.' })
    if (vote > 4 || vote < 0) return res.status(400).json({ message: 'Vote 0 and 4' })
    const verifyAndUpdate = await voteMedia(id, parseInt(vote), req.idUser, votesVerify.backVoted)

    statusMessage.data = verifyAndUpdate
    return res.status(200).json(statusMessage)
  }
}
