const routes = require('express').Router()
const { login, createAcc, deleteAcc, updateAcc } = require('./controllers/sessionController')
const { register, view, deleteMedias, updateMedias, voteMedias } = require('./controllers/mediasController')
const jsonwebtoken = require('../src/middleware/jwt')
const authentication = require('../src/middleware/authentication')
const { hasPermission, verifyAdm } = require('../src/middleware/hasPermissions')
const { medias, middlewareMedias, middlewareUpdateMedias, voteMiddlewareMedias } = require('../src/middleware/medias')

routes.route('/user/login').post(authentication.login, login)
routes.route('/user/register').post(hasPermission, authentication.register, createAcc)

routes.use(jsonwebtoken)

routes.route('/user').delete(deleteAcc).put(updateAcc)

routes.route('/medias').post(verifyAdm, medias, register).get(view).delete(verifyAdm, middlewareMedias, deleteMedias).put(verifyAdm, middlewareUpdateMedias, updateMedias)
routes.route('/medias/vote').post(voteMiddlewareMedias, voteMedias)

routes.use(verifyAdm)

module.exports = routes
