require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
})
const mongoose = require('mongoose')
mongoose.connect(
  process.env.URL + ':' + process.env.PORT_DB + '/' + process.env.DATABASE,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
  },
  (err) => (err ? console.log(err) : console.log('[DATABASE] Connected with successfully'))
)

module.exports = mongoose
