module.exports.STATUS_REQUEST = {
  401: {
    status: 401,
    message: 'Unauthorized'
  },
  404: {
    status: 404,
    message: 'Not found'
  },
  400: {
    status: 400,
    message: 'Bad request'
  }
}
