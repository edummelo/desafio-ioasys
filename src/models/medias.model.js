const { Schema, model, Types } = require('mongoose')
const mediasModal = new Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  type_media: {
    type: String,
    enum: ['series', 'movie'],
    required: true
  },
  thumbnail: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  trailer: {
    type: String,
    required: true
  },
  url_media: {
    type: String,
    required: true
  },
  min_age: {
    type: Number,
    required: true
  },
  genres: {
    type: String,
    required: true
  },
  studio: {
    type: String,
    required: true
  },
  director: {
    type: String,
    required: true
  },
  recomendations: {
    total: {
      type: Number,
      default: 0
    },
    stars: {
      type: Number,
      default: 0
    }
  },
  avaliations: {
    0: { type: Number, default: 0 },
    1: { type: Number, default: 0 },
    2: { type: Number, default: 0 },
    3: { type: Number, default: 0 },
    4: { type: Number, default: 0 }
  },
  enabled: {
    type: Boolean,
    default: true
  }
})

module.exports.verifyMedias = async (data) => {
  return this.db.findOne(data)
}

module.exports.deleteMedia = async (data) => {
  if (!Types.ObjectId.isValid(data.id)) return false
  const verify = await this.db.findOne({ _id: new Types.ObjectId(data.id) })
  if (!verify) return false
  return this.db.updateOne({ _id: new Types.ObjectId(data.id) }, { $set: { enabled: false } })
}

module.exports.registerMedia = async (data) => {
  const register = new this.db(data)
  return register.save()
}

module.exports.searchMedia = async (content, category = false) => {
  const filters = {}
  filters.enabled = true
  if (content) {
    if (category) filters[category] = { $regex: new RegExp('^' + content.toLowerCase(), 'i') }
    else filters.name = { $regex: new RegExp('^' + content.toLowerCase(), 'i') }
  }
  const consultFilter = await this.db.find(filters).select('name description type_media thumbnail trailer min_age genres studio avaliations director enabled recomendations')
  return consultFilter
}

module.exports.updateMedias = async (id, data) => {
  if (!Types.ObjectId.isValid(id)) return false
  return this.db.updateOne({ _id: new Types.ObjectId(id) }, { $set: data })
}

module.exports.voteMedia = async (id, vote, idUser, backVoted) => {
  if (!Types.ObjectId.isValid(id)) return false
  const dataVote = await this.db.findOne({ _id: new Types.ObjectId(id) })
  if (!dataVote) return false
  if (backVoted !== 0) dataVote.avaliations[backVoted] -= 1
  dataVote.avaliations[vote] += 1
  await dataVote.save()

  const maxVote = await this.db.findOne({ _id: new Types.ObjectId(id) })
  const sortTop = this.sortTop(Object.values(maxVote.avaliations))
  const indexTop = this.convertsInteger(Object.values(maxVote.avaliations)).findIndex((c) => c === sortTop[0])
  const starMax = this.sortTop(Object.entries(dataVote.avaliations))

  return this.db.updateOne(
    { _id: new Types.ObjectId(id) },
    {
      $set: {
        recomendations: {
          total: Object.values(maxVote.avaliations).reduce((a, b) => a + b),
          stars: starMax[0][0]
        }
      }
    }
  )
}

module.exports.sortTop = (array) => {
  return array.sort((a, b) => parseInt(b) - parseInt(a))
}

module.exports.convertsInteger = (array) => {
  let newArray = []
  array.forEach((values) => (!isNaN(parseInt(values)) ? newArray.push(parseInt(values)) : ''))
  return newArray
}

module.exports.db = model('Medias', mediasModal)
