const { Schema, model, Types } = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const UsersModel = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: [true, 'email is required!']
  },
  gender: {
    type: String,
    enum: ['male', 'female'],
    required: [true, 'gender is required']
  },
  password: {
    type: String,
    required: [true, 'password is required']
  },
  name: {
    first: {
      type: String,
      required: [true, 'Name first required']
    },
    last: {
      type: String,
      required: true
    }
  },
  permissions: {
    type: String,
    default: 'MEMBER',
    enum: ['MEMBER', 'ADM'],
    required: true
  },
  birth_date: {
    type: String,
    required: [true, 'Birth date is required']
  },
  avatar_url: String,
  enabled: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Number,
    required: [true, 'Created account is required']
  },
  medias: {
    favorites: Array
  },
  voted: {
    type: Number,
    default: 0
  }
})

module.exports.checkLogin = async (data) => {
  const findLogin = await this.db.findOne({ email: data.email, enabled: true })
  if (!findLogin) return findLogin
  if (!(await bcrypt.compare(data.password, findLogin.password.toString()))) return false
  return findLogin
}

module.exports.checkRegister = async (data) => {
  const findUser = await this.db.findOne({ email: data.email, enabled: true })
  return findUser
}

module.exports.createAccount = async (data) => {
  if (await this.db.findOne({ email: data.email })) {
    await this.db.deleteOne({ email: data.email })
  }
  const created = await this.db.create(data)
  return created
}

UsersModel.pre('save', async function (next) {
  if (!this.isModified('password')) return next()
  const salts = await bcrypt.genSalt(parseInt(process.env.SALT))
  const hash = await bcrypt.hash(this.password, salts)
  this.password = hash
  next()
})

module.exports.generateToken = async (object) => {
  return jwt.sign(object, process.env.APP_SECRET, { expiresIn: process.env.EXPIRES_TOKEN })
}

module.exports.deleteAccount = async (id) => {
  if (!Types.ObjectId.isValid(id)) return false
  return this.db.updateOne({ _id: new Types.ObjectId(id) }, { $set: { enabled: false } })
}

module.exports.updateAccount = async (id, data) => {
  if (!Types.ObjectId.isValid(id)) return false
  return this.db.updateOne({ _id: new Types.ObjectId(id) }, { $set: data })
}

module.exports.db = model('Users', UsersModel)
