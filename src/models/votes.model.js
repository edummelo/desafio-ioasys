const { Schema, model } = require('mongoose')

const VotesSchema = new Schema({
  media_id: {
    type: String
  },
  id_user: {
    type: String
  },
  voted: {
    type: Number,
    default: 0
  }
})

module.exports.verifyVoted = async (data) => {
  const hasVoted = await this.db.findOne({ media_id: data.id })
  const backVote = hasVoted ? hasVoted.voted : 0
  if (hasVoted) {
    if (hasVoted.voted !== data.vote) {
      hasVoted.voted = data.vote
      await hasVoted.save()
      return { status: false, backVoted: backVote }
    }
  }
  if (!hasVoted) {
    const dataDB = new this.db()
    dataDB.id_user = data.idUser
    dataDB.voted = data.vote
    dataDB.media_id = data.id
    await dataDB.save()
    return { status: false, backVote }
  }

  return { status: true, backVote }
}

module.exports.db = model('Votes', VotesSchema)
