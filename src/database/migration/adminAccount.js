;(async () => {
  require('dotenv').config({
    path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env'
  })
  require('../../config/database')
  const { Users } = require('../../models/index')
  const { connection } = require('mongoose')

  if (await Users.db.findOne({ email: 'eduardo@admin.com.br' })) return connection.close()

  const data = new Users.db()
  data.name = {
    first: 'Eduardo',
    last: 'Melo'
  }

  data.email = 'eduardo@admin.com.br'
  data.password = '123456'
  data.gender = 'male'
  data.created_at = new Date().getTime()
  data.birth_date = '25-10-1998'
  data.permissions = 'ADM'

  console.log('[MIGRATION] - Loading...')
  const registered = await data.save()
  console.log('[MIGRATION] - Created successfully account administrator.')
  console.log('your password: 123456')
  console.log(registered)

  connection.close()
})()
