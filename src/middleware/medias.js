const { body, validationResult } = require('express-validator')
module.exports.medias = [
  body('name').notEmpty().withMessage('Name required.'),
  body('description').notEmpty().withMessage('Description required.'),
  body('type_media').notEmpty().withMessage('Trailer required.'),
  body('description').notEmpty().withMessage('Description required.'),
  body('trailer').notEmpty().withMessage('Trailer required.'),
  body('studio').notEmpty().withMessage('Studio required.'),
  body('min_age').notEmpty().withMessage('Minimum age required.'),
  body('genres').notEmpty().withMessage('Genres required'),
  body('url_media').notEmpty().withMessage('URL Media required.'),
  async (req, res, next) => {
    const { type_media } = req.body
    const media = type_media ? type_media.toLowerCase() : type_media
    switch (media) {
      case 'movie':
      case 'series':
        next()
        break
      default:
        const messageError = {
          msg: 'Type media movie or series.',
          param: 'type_media',
          location: 'body'
        }
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
          const errorsAsign = errors.array()
          return res.status(400).json([errorsAsign, messageError].flat())
        }
        res.status(400).json(messageError)
        break
    }
  }
]

module.exports.middlewareMedias = [body('id').notEmpty().withMessage('Id required.')]

module.exports.middlewareUpdateMedias = [body('id').notEmpty().withMessage('Id required.'), body('update').notEmpty().withMessage('Update required.')]

module.exports.voteMiddlewareMedias = [body('id').notEmpty().withMessage('Id required.'), body('vote').notEmpty().withMessage('Vote required')]
