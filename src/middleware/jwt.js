const jwt = require('jsonwebtoken')
const { promisify } = require('util')
const { STATUS_REQUEST } = require('../constants')
module.exports = async (req, res, next) => {
  const { authorization } = req.headers

  if (!authorization) return res.status(STATUS_REQUEST[401].status).send(STATUS_REQUEST[401])

  const [, token] = authorization.split(' ')

  try {
    const decoded = await promisify(jwt.verify)(token, process.env.APP_SECRET)
    req.permissions = decoded.permissions
    req.idUser = decoded.id
    return next()
  } catch (err) {
    return res.status(STATUS_REQUEST[401].status).json(STATUS_REQUEST[401])
  }
}
