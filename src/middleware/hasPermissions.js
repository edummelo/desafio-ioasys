module.exports.verifyAdm = async (req, res, next) => {
  if (!['ADM'].includes(req.permissions)) {
    return res.status(403).json({
      message: 'Forbidden'
    })
  }
  return next()
}

module.exports.hasPermission = async (req, res, next) => {
  const { permissions } = req.body
  if (['ADM'].includes(permissions)) {
    return res.status(403).json({
      message: 'Forbidden'
    })
  }
  return next()
}
