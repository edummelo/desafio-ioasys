const { body } = require('express-validator')
const { STATUS_REQUEST } = require('../constants')

module.exports.register = [
  body('email').isEmail().withMessage('Email is not valid.'),
  body('email').notEmpty().withMessage('Email is required.'),
  body('password').notEmpty().withMessage('Password is not empty.'),
  body('gender').notEmpty().withMessage('Gender is not empty.'),
  body('name.first').notEmpty().withMessage('First name is not empty.'),
  body('name.last').notEmpty().withMessage('Last name is not empty.'),
  body('gender').notEmpty().withMessage('Gender is not empty.'),
  body('birth_date').notEmpty().withMessage('Birth date is not empty.')
]

module.exports.login = [body('email').isEmail().withMessage('Email is not valid'), body('password').notEmpty().withMessage('Password is not empty')]
