const server = require('./app')
require('./config/database')
server.listen(process.env.PORT || 3000, () => console.log('[API] initialized successfully listening port', process.env.PORT))
