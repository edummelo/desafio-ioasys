# Desáfio Back-end

Desáfio back-end, construir uma api rest no qual a plataforma **IMDb** irá consumir.

## Bibliotecas de desenvolvimento que foram utilizados

- [Jest](https://jestjs.io/pt-BR/) (TDD)
- [Standard](https://standardjs.com/) (ESlint)
- [Nodemon](https://www.npmjs.com/package/nodemon)
- [Prettier](https://prettier.io/) (Code Formater)
- [Supertest](https://www.npmjs.com/package/supertest) (Testes de integração junto ao Jest)

## Bibliotecas em produção

- [Bcrypt](https://www.npmjs.com/package/bcryptjs)
- [Cors](https://www.npmjs.com/package/cors)
- [dotenv](https://www.npmjs.com/package/dotenv)
- [Express](https://www.npmjs.com/package/express)
- [Express-validator](https://express-validator.github.io/docs/index.html)
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
- [mongoose](https://mongoosejs.com/)

## Como executar os demais comandos

- instalação de pacotes

```bash
    npm install
```

- Comandos de execução no projeto.

* Iniciando o projeto em produção

```bash
    npm start
```

- Iniciando o projeto em desenvolvimento

```bash
    npm run dev
```

- Iniciando os testes utilizados em jest

```bash
    npm run test
```

- Iniciando a verificação no código com prettier

```bash
    npm run formatter:check
```

- Iniciando a correção de indentação e afins com prettier

```bash
    npm run formatter:write
```

- Iniciando teste com lint standard

```bash
    npm run lint
```

- Gerando conta admin.

```bash
    npm run migration:admin
```

- Gerando conta admin em banco de dados dev (.env.test).

```bash
    npm run migration:admin-dev
```

[Como usar](https://bitbucket.org/edummelo/desafio-ioasys/src/master/DOCUMENTATION.md)
