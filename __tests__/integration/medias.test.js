const request = require('supertest')
const app = require('../../src/app')
const mongoose = require('mongoose')
const truncate = require('../util/truncate')
const { Users, Medias } = require('../../src/models/index')

beforeAll((done) => {
  require('../../src/config/database')
  done()
})

beforeEach(async () => {
  await truncate()

  const data = new Medias.db()
  data.name = 'Piratas do Caribe1998'
  data.type_media = 'movie'
  data.thumbnail = 'http://cdn.localhost/imagem.png'
  data.description = 'lorem ipsum'
  data.trailer = 'http://media.localhost/video.mkv'
  data.url_media = 'http://media.localhost/video.mkv'
  data.director = 'Espen Sandberg, Joachim Rønning, Gore Verbinski, Rob Marshall'
  data.studio = 'Walt Disney'
  data.genres = 'Aventura, Ação'
  data.min_age = 16
  await data.save()

})

let token

describe('Registration Medias', () => {
  it('Registering data, but some in blank for return status 400', async () => {
    const data = {}
    data.name = 'Piratas do Caribe'
    data.type_media = 'movie'
    data.thumbnail = 'http://cdn.localhost/imagem.png'
    data.description = 'lorem ipsum'
    data.trailer = 'http://media.localhost/video.mkv'
    data.url_media = 'http://media.localhost/video.mkv'
    data.categorys = 'Aventura Ação'
    data.director = 'Espen Sandberg, Joachim Rønning, Gore Verbinski, Rob Marshall'

    token = await this.token()

    const response = await request(app).post('/medias').send(data).set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(400)
  })

  it('Register data movie', async () => {
    const data = {}
    data.name = 'Piratas do Caribe0'
    data.type_media = 'movie'
    data.thumbnail = 'http://cdn.localhost/imagem.png'
    data.description = 'lorem ipsum'
    data.trailer = 'http://media.localhost/video.mkv'
    data.url_media = 'http://media.localhost/video.mkv'
    data.min_age = '16'
    data.genres = 'Aventura Ação'
    data.studio = 'Walt Disney Pictures'
    data.director = 'Espen Sandberg, Joachim Rønning, Gore Verbinski, Rob Marshall'

    const response = await request(app).post('/medias').send(data).set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(200)
  })

  it('List all movies or series', async () => {

    const response = await request(app)
      .get('/medias')
      .send({
        content: 'Piratas do Caribe'
      })
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(200)
  })

  it('Delete movies or series', async () => {
    const data = new Medias.db()
    data.name = 'Piratas do Caribe1'
    data.type_media = 'movie'
    data.thumbnail = 'http://cdn.localhost/imagem.png'
    data.description = 'lorem ipsum'
    data.trailer = 'http://media.localhost/video.mkv'
    data.url_media = 'http://media.localhost/video.mkv'
    data.director = 'Espen Sandberg, Joachim Rønning, Gore Verbinski, Rob Marshall'
    data.studio = 'Walt Disney'
    data.genres = 'Aventura, Ação'
    data.min_age = 16
    const saveData = await data.save()

    const response = await request(app).delete('/medias').send({ id: saveData._id }).set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(200)
  })

  it('try delete invalid data return status 404', async () => {

    const response = await request(app).delete('/medias').send({ id: '21312321' }).set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(404)
  })

  it('Update data medias', async () => {
    const data = new Medias.db()
    data.name = 'Piratas do Caribe2'
    data.type_media = 'movie'
    data.thumbnail = 'http://cdn.localhost/imagem.png'
    data.description = 'lorem ipsum'
    data.trailer = 'http://media.localhost/video.mkv'
    data.url_media = 'http://media.localhost/video.mkv'
    data.director = 'Espen Sandberg, Joachim Rønning, Gore Verbinski, Rob Marshall'
    data.studio = 'Walt Disney'
    data.genres = 'Aventura, Ação'
    data.min_age = 16
    const saveData = await data.save()

    const response = await request(app)
      .put('/medias')
      .send({
        id: saveData._id,
        update: { name: 'Eduardo Melo filosofal' }
      })
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(200)
  })

  it('Vote a content give points recomendation 0 and 4', async () => {

    const data = await Medias.db.findOne({ name: 'Piratas do Caribe1998' })

    const response = await request(app)
      .post('/medias/vote')
      .send({
        id: data._id,
        vote: 4
      })
      .set('Authorization', `Bearer ${token}`)

    expect(response.status).toBe(200)
  })
})

module.exports.token = async () => {
  return Users.generateToken({ id: '12388', email: 'eduardo@admin.com.br', permissions: 'ADM' })
}

afterEach(async () => {

  await Medias.db.deleteOne({ name: 'Piratas do Caribe1998' })
  await truncate()
})

afterAll((done) => {
  mongoose.connection.close()
  done()
})
