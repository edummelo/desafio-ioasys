const request = require('supertest')
const app = require('../../src/app')
const mongoose = require('mongoose')
const truncate = require('../util/truncate')
const { Users } = require('../../src/models/index')

beforeAll((done) => {
  require('../../src/config/database')
  done()
})

beforeEach(async () => {
  await truncate()

  const register = new Users.db()
  register.name = {
    first: 'Eduardo',
    last: 'Melo'
  }
  register.email = 'eduardo0@admin.com.br'
  register.password = '123123'
  register.gender = 'male'
  register.created_at = new Date().getTime()
  register.birth_date = '25-10-1998'
  await register.save()

})




describe('Authentication', () => {
  it('case send invalid data return status 401', async () => {
    const response = await request(app).post('/user/login').send({
      email: 'eduardo@hotmail.com',
      password: '123'
    })

    expect(response.status).toBe(401)
  })

  it('authenticating with valid data', async () => {

    const response = await request(app).post('/user/login').send({
      email: 'eduardo0@admin.com.br',
      password: '123123'
    })

    expect(response.status).toBe(200)
  })

  it('login for access private routes return authorization token', async () => {

    const response = await request(app).post('/user/login').send({
      email: 'eduardo0@admin.com.br',
      password: '123123'
    })

    expect(response.body).toHaveProperty('authentication.token')
  })
})

it('force list medias without token bearer', async () => {

  const response = await request(app).get('/medias')
  .set('Authorization', `Bearer 21232`)
  
  expect(response.status).toBe(401)
})

describe('Registration', () => {
  it('register a new user in the database', async () => {
    const create = {}
    create.name = {
      first: 'Eduardo',
      last: 'Melo'
    }
    create.email = 'eduardo.melo1@email.com'
    create.password = '123456789'
    create.gender = 'male'
    create.birth_date = '25-10-1998'

    const response = await request(app).post('/user/register').send(create)

    expect(response.status).toBe(200)
  })

  it('register with invalid data for return status 400', async () => {
    const create = {}
    create.name = {
      first: 'Eduardo',
      last: 'Melo'
    }
    create.password = '123456789'
    create.gender = 'male'

    const response = await request(app).post('/user/register').send(create)

    expect(response.status).toBe(400)
  })

  it('if force set permission adm return status 403', async () => {
    const create = {}
    create.name = {
      first: 'Eduardo',
      last: 'Melo'
    }
    create.email = 'eduardo.melo2@email.com'
    create.password = '123456789'
    create.gender = 'male'
    create.birth_date = '25-10-1998'
    create.permissions = 'ADM'

    const response = await request(app).post('/user/register').send(create)

    expect(response.status).toBe(403)
  })
})

module.exports.generatedPass = () => {
  return Math.floor(Math.random() * 234323324)
}

afterEach(async () => {

  await Users.db.deleteOne({ email: 'eduardo0@admin.com.br' })

  await truncate()
})

afterAll((done) => {
  mongoose.connection.close()
  done()
})
