const bcrypt = require('bcryptjs')
const mongoose = require('mongoose')
const { Users, Medias } = require('../../src/models/index')
const truncate = require('../util/truncate')

beforeAll((done) => {
  require('../../src/config/database')
  done()
})

beforeEach(async () => {
  await truncate()
})

describe('User', () => {
  it('encrypt password of user', async () => {
    const register = new Users.db()
    register.name = {
      first: 'Eduardo',
      last: 'Melo'
    }

    register.email = 'eduardo@admin.com.br'
    register.password = '123456'
    register.gender = 'male'
    register.created_at = new Date().getTime()
    register.birth_date = '25-10-1998'
    await register.save()

    const compareHash = await bcrypt.compare('123456', register.password)

    expect(compareHash).toBe(true)
  })

  it('Verify max number', async () => {
    const topRecomendation = await Medias.sortTop([1, 2, 3, 4, 5, 6, 7, 8])
    expect(topRecomendation[0]).toBe(8)
  })
})

afterEach(async () => {
  await truncate()
})

afterAll((done) => {
  mongoose.connection.close()
  done()
})
